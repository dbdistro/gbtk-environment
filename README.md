# Gameboy Developer Kit Environment

<p>Installs GBDK-2020 as well as other useful programs for building a gameboy rom.</p>

[GBDK-2020](https://github.com/gbdk-2020/gbdk-2020)<br>
[GBTD and GBMB 2020](https://github.com/gbdk-2020/GBTD_GBMB)<br>
[GB SFX Generator](https://patchworkgames.itch.io/gbsfx)<br>
[Gearboy](https://github.com/drhelius/Gearboy)<br>
[GameBoyPngConverter](https://github.com/gingemonster/GameBoyPngConverter)<br>

## Table of Context
[What does this install?](#what-does-this-install)<br>
[Dependencies](#dependencies)<br>
[Install](#install)<br>
[Setup your project](#setup-your-project)<br>


## What does this install?
### GBDK-2020
Compiles your C code into a form that the gameboy can recognize.

### GBTD [GameBoy Tile Designer]
Allows you to easily create and import sprites into your game.<br><br>I would recommend first creating the sprite in an external drawing program like gimp. Then copy and paste the sprite into this program. GBDK-2020 can't read 16x16 sprites, so you'll have to create a mega-sprite from 8x8 sprite size. This program will take 16x16 sprites and split them up into seperate 8x8 sprites.

### GBMB [GameBoy Map Builder]
Creates the background layer from your tile sheet created with GBTD.

### GB SFX Generator [GameBoy Sound Effect Generator]
This program will launch GearBoy with this gameboy rom that can be used to create sound effects easily. Enable the debugger tool in GearBoy and put a check by `Debug > Show Sound Registers`.

### GearBoy
Gearboy is a cross-platform Game Boy / Game Boy Color emulator written in C++ that runs on Windows, macOS, Linux, BSD and RetroArch.

### GameBoyPngConverter
A image converter that turns pngs into a background layer. Note that this program is ran via command line. To use type the command `GameBoyPngConverter`.


## Dependencies

- gearboy
- wine
- libgdiplus


## Install
### Arch Linux
Make sure your <code>/etc/pacman.conf</code> contains "dbDistro-repo".
If not follow the instructions [here](https://gitlab.com/dbdistro/package-repos/pacman-repo).<br>


``` bash
sudo pacman -Sy gbdk-environment
```

## Setup your project
Copy the file:
``` bash
/usr/share/doc/gbdk-environment/gbdk/gameBoyProjectSetupExample/Makefile
```
into your projects dir and type `make` when ready to compile your gameboy rom.
